# Countries API v1.0

## Requirements

Libraries required:
* `django = "2.2.2"` (https://www.djangoproject.com/)
* `djangorestframework = "3.9.4"` (https://www.django-rest-framework.org/)
* `djongo = "1.2.33"` (https://nesdis.github.io/djongo/)
* `mongodb = 4.0.3` (https://www.mongodb.com/)
* `markdown = "3.1.1"`
* `coreapi = "2.3.3"`
* `django-filter = "2.1.0"`

Mongo must be running in:
* `HOST = 127.0.0.1`
* `PORT = 26099`

However these settings could be changed in `countries_api/settings.py` config file.

## Installation
Clone repo

`git clone https://gitlab.com/jhoniscoding/countries_api.git`

Go into project folder

`cd countries_api`

Create virtual environment and install libraries ([Pipenv](https://docs.pipenv.org/en/latest/) is required)

`pipenv sync`

Open a terminal in the virtual environment

`pipenv shell`

Run the migration scripts (Make sure the mongo server is running)

`python manage.py migrate` 

That's all!

## Usage
To start the server run the following command:

`python manage.py runserver 8000`

Open the _Web browsable API_ in your browser at [http://127.0.0.1:8000/v1](http://127.0.0.1:8000/v1) 
to start using the API. Also it is possible to make requests to API using another REST clients like 
[Postman](https://www.getpostman.com/), [cURL](https://curl.haxx.se/), [Insomnia](https://insomnia.rest/), etc.

## Docs

Docs are exposed in [http://127.0.0.1:8000/docs/](http://127.0.0.1:8000/docs/).

# Code challenge
## Description
Build a Python application that receives the number of a column and return its conversion into letters, in the same way that spreadsheet does, 
*BUT* adding the spanish letter (_Ñ_).

## Usage
`python convert_int_to_column.py`

Once the app starts you have to input one of the following options:

- `help`: Prints help info
- _Comma separated number list_: Number list with numbers to be converted. 
    - `1,2,3,4` will return `A B C D`
- _Range_: Numbers array build from start to end (include). 
    - `2..4` will return `B C D`
- Number: Number to be converted. 
    - `2` will be converted into `B`
- `ia`: I-th alphabet: Alphabet computed for the given `i` 
    - `1a` will return `A B C D E F G H I J K L M N Ñ O P Q R S T U V W X Y Z `
    - `2a` will return `AA AB AC AD AE AF AG AH AI AJ AK AL AM AN AÑ AO AP AQ AR AS AT AU AV AW AX AY AZ `
    - `3a` will return `BA BB BC BD BE BF BG BH BI BJ BK BL BM BN BÑ BO BP BQ BR BS BT BU BV BW BX BY BZ `
- `exit`: Finish the program execution