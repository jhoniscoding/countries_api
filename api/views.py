# Create your views here.
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import viewsets, status
from rest_framework.response import Response

from api.models import Region, Municipio
from api.serializers import RegionSerializer, MunicipioSerializer


class RegionViewSet(viewsets.ModelViewSet):
    """
    list:
    Retrieve regions list

    create:
    Create a new region with the given params

    retrieve:
    Retrieve region info for the given id

    destroy:
    Delete region with the given id

    update:
    Update region info with the given params

    partial_update:
    Update region fields with the given params
    """
    queryset = Region.objects.all().order_by('-created_at')
    serializer_class = RegionSerializer
    filter_backends = (DjangoFilterBackend,)
    filterset_fields = ('name', 'code')

    def find_inactive_muns(self, *args, **kwargs):
        serializer = self.get_serializer(*args, **kwargs)
        serializer.is_valid(raise_exception=True)
        muns = [mun.name for mun in serializer.validated_data["municipios"] if not mun.active]
        return serializer, bool(muns), muns

    def create(self, request, *args, **kwargs):
        serializer, inactive_found, muns = self.find_inactive_muns(data=request.data)
        if inactive_found:
            return Response(
                data={
                    "message": "Some Municipios are not active",
                    "municipios": muns
                },
                status=status.HTTP_400_BAD_REQUEST
            )
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)

    def update(self, request, *args, **kwargs):
        partial = kwargs.pop('partial', False)
        instance = self.get_object()
        serializer, inactive_found, muns = self.find_inactive_muns(instance, data=request.data, partial=partial)
        if inactive_found:
            return Response(
                data={
                    "message": "Some Municipios are not active",
                    "municipios": muns
                },
                status=status.HTTP_400_BAD_REQUEST
            )

        self.perform_update(serializer)

        if getattr(instance, '_prefetched_objects_cache', None):
            instance._prefetched_objects_cache = {}

        return Response(serializer.data)


class MunicipioViewSet(viewsets.ModelViewSet):
    """
    list:
    Retrieve municipios list

    create:
    Create a new municipio with the given params

    retrieve:
    Retrieve municipio info for the given id

    destroy:
    Delete municipio with the given id

    update:
    Update municipio info with the given params

    partial_update:
    Update municipio fields with the given params
    """
    queryset = Municipio.objects.all().order_by('-created_at')
    serializer_class = MunicipioSerializer
    filter_backends = (DjangoFilterBackend,)
    filterset_fields = ('name', 'code', 'active')

    def update(self, request, *args, **kwargs):
        response = super(MunicipioViewSet, self).update(request, *args, **kwargs)
        instance = self.get_object()
        if not instance.active:
            instance.region_set.clear()
        return response
