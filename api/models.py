from django.db import models

# Create your models here.
from django.utils import timezone


class Municipio(models.Model):
    code = models.IntegerField(unique=True)
    name = models.CharField(max_length=255)
    active = models.BooleanField(default=True)
    created_at = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return self.name


class Region(models.Model):
    code = models.IntegerField(unique=True)
    name = models.CharField(max_length=255)
    municipios = models.ManyToManyField(Municipio, blank=True)
    created_at = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return self.name
