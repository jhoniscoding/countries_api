from rest_framework import serializers

from api.models import Region, Municipio


class RegionSerializer(serializers.HyperlinkedModelSerializer):
    created_at = serializers.ReadOnlyField()

    class Meta:
        model = Region
        fields = ('url', 'code', 'name', "municipios", "created_at")


class MunicipioSerializer(serializers.HyperlinkedModelSerializer):
    created_at = serializers.ReadOnlyField()

    class Meta:
        model = Municipio
        fields = ('url', 'code', 'name', "active", "created_at")
