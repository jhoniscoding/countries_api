import sys


class CustomException(BaseException):

    NEGATIVE_NUMBER_FOUND = 1

    def __init__(self, message, code):
        self.message = message
        self.code = code


class Alphabet:
    """
    Class to represent an alphabet
    """

    def __init__(self, include_enie=False):
        self.include_enie = include_enie  # Include Ñ char in alphabet
        self.chars = self.build_alphabet()  # Alphabet symbols

    @classmethod
    def _build_char_array(cls, start_char, end_char):
        """
        Builds symbols array
        :param start_char: Start character
        :param end_char: End character
        :return: List <string>. Symbols array
        """
        return [chr(i) for i in range(ord(start_char), ord(end_char)+1)]

    def build_alphabet(self):
        """
        Builds alphabet symbols
        :return: List <string>. Alphabet symbols
        """
        a_to_n = self._build_char_array("A", "N")
        o_to_z = self._build_char_array("O", "Z")
        alphabet = []
        alphabet.extend(a_to_n)
        alphabet.extend(o_to_z)
        if self.include_enie:
            alphabet.insert(14, "Ñ")
        return alphabet

    def length(self):
        """
        Compute symbols array length
        :return: Int. Symbols array length
        """
        return len(self.chars)

    def get_char(self, index):
        """
        Get symbol at the index position
        :param index: Index position
        :return: String. Symbol found at the index position
        """
        return self.chars[index]


def convert(m, alphabet=Alphabet(include_enie=True)):
    """
    Converts the given number into the corresponding string according with the given alphabet
    :param m: Number to be converted
    :param alphabet: Optional. Alphabet object
    :return: String. Corresponding string for the given number
    """
    chars = []
    n = m - 1  # The symbols array is zero index
    if n < 0:
        raise CustomException(
            "Only positive integers are allowed!", code=CustomException.NEGATIVE_NUMBER_FOUND
        )
    elif n < alphabet.length():
        # If number is smaller than alphabet length there is no need to process
        return alphabet.get_char(n)

    # Retrieve quotient and remainder for the number and alphabet length
    n, r = divmod(n, alphabet.length())

    # Insert remainder symbol
    chars.insert(0, alphabet.get_char(r))

    # Process quotient and insert result into symbols array
    chars.insert(0, convert(n))

    # Join all the symbols
    return "".join(chars)


def process(inpt):
    """
    Process menu option given by the user
    :param inpt: String option
    :return:
    """
    if "," in inpt:
        rng = [int(n) for n in inpt.split(",")]
    elif ".." in inpt:
        rng = range(*[int(n) for n in inpt.split("..")])
    elif "a" in inpt:
        n = 27 * (int(inpt.split("a")[0]) - 1)
        rng = range(n+1, n+28)
    elif inpt == "help":
        print(
            "Range could be:"
            "\n\t- comma separated number list: a,b,c"
            "\n\t- start value and end value: a..b"
            "\n\t- i-th alphabet: 1a, 2a, 3a"
            "\nType 'exit' to quit",
            end=" "
        )
        return
    elif inpt == "exit":
        print("Bye!")
        sys.exit()
    else:
        print(convert(int(inpt)), end=" ")
        return

    for x in rng:
        print(convert(x), end=" ")


if __name__ == '__main__':
    while True:
        inpt = input("Please enter a number, range or i-th alphabet. (Type help to see possible inputs): ")
        try:
            process(inpt)
            print("\n")
        except ValueError as ex:
            print("Only integer numbers are allowed!")
        except CustomException as ex:
            if ex.code == CustomException.NEGATIVE_NUMBER_FOUND:
                print(ex.message)
